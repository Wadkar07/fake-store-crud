import React from 'react';
import './AddProduct.css'
import uuid4 from 'uuid4';
import validateForm from './validateForm';

export default class AddProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = { errors: {}, submitError: '', productAdded: false }
    }
    productDataModel = {
        category: "",
        description: "",
        id: uuid4(),
        image: "",
        price: '',
        rating: { count: '', rate: '' },
        title: ""
    }
    onInputChange = (event) => {
        let errorMessage = validateForm(event);
        this.setState({
            errors: {
                ...this.state.errors,
                [event.target.name]: errorMessage
            }
        })
        const { name, value } = event.target;
        if (name === "rate" || name === "count") {
            this.productDataModel['rating'][name] = value;
        }
        else {
            this.productDataModel[name] = value;
        }
    }
    addProduct = () => {
        let numberOfError = Object.values(this.state.errors).filter((error) => {
            return error;
        })
        if (numberOfError.length === 0) {
            this.setState({ productAdded: true })
            console.log('added');
            this.setState({ submitError: '' });
            this.props.addProduct(this.productDataModel)
        }
        else {
            this.setState({ submitError: 'Please Fill the details correctly' });
        }
    }
    render() {
        return (
            <div>
                <details name="add">
                    <summary>Add Product</summary>
                    {
                        this.state.productAdded ?
                            <div className="product-added">
                                <p>Product Added SuccessFully</p>
                                <button
                                    onClick={() => {
                                        this.setState({ productAdded: false })
                                    }}
                                >
                                    Add More
                                </button>
                            </div>
                            :

                            <form className='add-form' onSubmit={(event) => {
                                event.preventDefault();
                                this.addProduct();
                            }}>
                                <div className="product-name form-row">
                                    <label>Enter Product Name</label>
                                    <input required name="title" type="text" onChange={(event) => {
                                        this.onInputChange(event)
                                    }
                                    } />

                                </div>
                                {this.state.errors.title && <small>{this.state.errors.title}</small>}
                                <div className="product-category form-row">
                                    <label>Enter Product Category</label>
                                    <input required name="category" type="text" onChange={(event) => {
                                        this.onInputChange(event)
                                    }
                                    } />
                                </div>
                                {this.state.errors.category && <small>{this.state.errors.category}</small>}

                                <div className="product-price form-row">
                                    <label>Enter Product Price</label>
                                    <input required name="price" type="text" onChange={(event) => {
                                        this.onInputChange(event)
                                    }
                                    } />
                                </div>
                                {this.state.errors.price && <small>{this.state.errors.price}</small>}

                                <div className="product-rating form-row">
                                    <label>Enter Product Rating</label>
                                    <input required name="rate" type="text" onChange={(event) => {
                                        this.onInputChange(event)
                                    }
                                    } />
                                </div>
                                {this.state.errors.rate && <small>{this.state.errors.rate}</small>}


                                <div className="rating-count form-row">
                                    <label>Enter Product Rating Count</label>
                                    <input required name="count" type="number" onChange={(event) => {
                                        this.onInputChange(event)
                                    }
                                    } />
                                </div>
                                {this.state.errors.count && <small>{this.state.errors.count}</small>}

                                <div className="product-description form-row">
                                    <label>Enter Product description</label>
                                    <textarea rows="5" name="description" type="text" onChange={(event) => {
                                        this.onInputChange(event)
                                    }
                                    } />
                                </div>
                                {this.state.errors.description && <small>{this.state.errors.description}</small>}


                                <div className="product-name form-row">
                                    <label>Enter Product Image Link</label>
                                    <input required name="image" type="text" onChange={(event) => {
                                        this.onInputChange(event)
                                    }
                                    } />
                                </div>


                                <input className="addproduct-submit" type="submit" value="Add" />
                                {this.state.submitError && <small>{this.state.submitError}</small>}

                            </form>
                    }
                </details>
            </div >
        );
    }
}