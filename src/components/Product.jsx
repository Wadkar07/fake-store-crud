import React, { Component } from 'react'
import './Product.css'
import validateForm from './validateForm';

export default class Product extends Component {
    constructor(props) {
        super(props);
        this.state = { errors: {}, request: false, deleteRequest: false }
    }
    productDataModel = {
        category: "",
        description: "",
        id: "",
        image: "",
        price: '',
        rating: { count: '', rate: '' },
        title: ""
    }

    updateChanges = (event, product) => {
        let errorMessage = validateForm(event);
        this.setState({
            errors: {
                ...this.state.errors,
                [event.target.name]: errorMessage
            }
        })
        const { name, value } = event.target;
        if (name === "rate" || name === "count") {
            this.productDataModel['rating'][name] = value;
        }
        else {
            this.productDataModel[name] = value;
        }
        this.props.updateProduct(this.productDataModel, name, product.id);
    }

    updateProduct = () => {
        this.setState({ request: true });
    }

    confirmUpdate = (event) => {
        event.preventDefault();
        this.setState({ request: false });
    }

    cancelUpdate = () => {
        this.setState({ request: false });
    }

    deletePrompt = (event) => {
        event.preventDefault();
        console.log("in delete prompt")
        this.setState({ deleteRequest: true });
    }

    deleteProduct(product) {
        this.props.deleteProduct(product);
    }

    render() {
        let product = this.props.product;
        return (
            <div className='product'>
                {
                    this.state.request ?
                        <>
                            <div className="image-div">
                                <img src={product.image} alt="" />
                            </div>
                            <form
                                onSubmit={(event) => {
                                    this.confirmUpdate(event);
                                }}
                                className='update-form' >
                                <div className="product-details">
                                    <div className="update-form-row">
                                        <input
                                            placeholder="Title"
                                            name="title"
                                            required
                                            value={product.title}
                                            type="text"
                                            onChange={(event) => {
                                                this.updateChanges(event, product)
                                            }
                                            } />
                                    </div>
                                    {this.state.errors.title && <small>{this.state.errors.title}</small>}

                                    <div className="update-form-row">
                                        <input
                                            placeholder="Category"
                                            name="category"
                                            required
                                            value={product.category}
                                            type="text"
                                            onChange={(event) => {
                                                this.updateChanges(event, product)
                                            }
                                            } />
                                    </div>
                                    {this.state.errors.category && <small>{this.state.errors.category}</small>}


                                    <div className="update-form-row">
                                        <input
                                            placeholder="Price"
                                            name="price"
                                            required
                                            value={product.price}
                                            type="number"
                                            onChange={(event) => {
                                                this.updateChanges(event, product)
                                            }
                                            } />
                                    </div>
                                    {this.state.errors.price && <small>{this.state.errors.price}</small>}

                                    <div className="update-form-row">
                                        <input
                                            placeholder="Rating"
                                            name="rate"
                                            required
                                            value={product.rating.rate}
                                            type="number"
                                            onChange={(event) => {
                                                this.updateChanges(event, product)
                                            }
                                            } />
                                    </div>
                                    {this.state.errors.rate && <small>{this.state.errors.rate}</small>}

                                    <div className="update-form-row">
                                        <input
                                            placeholder="Rating Count"
                                            name="count"
                                            required
                                            value={product.rating.count}
                                            type="number"
                                            onChange={(event) => {
                                                this.updateChanges(event, product)
                                            }
                                            } />
                                    </div>
                                    {this.state.errors.count && <small>{this.state.errors.count}</small>}

                                    <div className="product-description update-form-row">
                                        <textarea rows="5"
                                            name="description"
                                            required
                                            value={product.description}
                                            type="text"
                                            onChange={(event) => {
                                                this.updateChanges(event, product)
                                            }
                                            } />
                                    </div>
                                    {this.state.errors.description && <small>{this.state.errors.description}</small>}

                                    <div className="update-form-row">
                                        <input
                                            placeholder="Image Link"
                                            name="image"
                                            value={product.image}
                                            required
                                            type="text" onChange={(event) => {
                                                this.updateChanges(event, product)
                                            }
                                            } />
                                    </div>
                                </div>
                                <div className="operations">
                                    <input className='update-btn' type="submit" value="submit" />
                                </div>
                            </form>
                        </>
                        :
                        <>
                            <div className="image-div">
                                <img src={product.image} alt="" />
                            </div>
                            <div className="product-details">
                                <p className='product-title'>{product.title}</p>
                                <p className='product-category'>{product.category}</p>
                                <p className='product-price'>Price: {product.price}$</p>
                                <p className='product-rating-rate'>Rating : {product.rating.rate}</p>
                                <p className='product-rating-count'>Rating Count : {product.rating.count}</p>
                                <p className='product-description'>{product.description}</p>
                            </div>
                            <div className="operations">
                                {this.state.deleteRequest ?
                                    <>
                                        <p className='delete-prompt'>Do You want to delete this product?</p>
                                        <button onClick={() => {
                                            this.deleteProduct(product)
                                        }}>YES</button>
                                        <button onClick={() => {
                                            this.setState({ deleteRequest: false });
                                        }}>NO</button>
                                    </>
                                    :
                                    <>
                                        <button onClick={() => {
                                            this.updateProduct();
                                        }}>Update</button>

                                        <button onClick={(event) => {
                                            this.deletePrompt(event);
                                        }}>Delete</button>
                                    </>
                                }
                            </div>
                        </>
                }
            </div>
        )
    }
}
