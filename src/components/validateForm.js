import validator from "validator";

export default function validateForm(event){
    const {name, value} = event.target;
    if(name==='title' && (value.length > 100 || value.length < 5))
    {
        return 'Product name must be minimum 5 characters and maximum 100 characters'
    }
    else if(name==='category' && (value.length >20 || value.length < 5)){
        return 'Category must be minimum 5 characters and maximum 20 characters'
    }
    else if(name==='price' && !validator.isNumeric(value)){
        return 'Enter only number';
    }
    else if(name==='rate' && (value >=6 || value <0)) {
        return 'Enter valid rating from 1-5'
    }
    else if(name==='description' && (value.length <10)){
        return 'Please enter enough description'
    }
    else{
        return '';
    }
}