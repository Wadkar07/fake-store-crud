import React, { Component } from 'react'
import Product from './Product'

export default class Products extends Component {
  render() {
    return (
      <div>
        {
          this.props.products.map((product) => {
            return(
              <Product product={product} updateProduct={this.props.updateProduct} deleteProduct={this.props.deleteProduct} key={product.id}/>
            )
          })
        }
      </div>
    )
  }
}
