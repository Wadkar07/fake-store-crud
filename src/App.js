import axios from 'axios';
import React, { Component } from 'react'
import Products from './components/Products';
import AddProduct from './components/AddProduct';
import PreLoader from './components/PreLoader';
import Error from './components/Error';

export default class App extends Component {
  constructor(props) {

    super(props);

    this.STATE_API = {
      LOADING: 'loading',
      LOADED: 'loaded',
      ERROR: 'error'
    }

    this.state = {
      products: [],
      status: this.STATE_API.LOADING
    }
    this.URL = 'https://fakestoreapi.com/products';
  }

  updateProduct = (product, name, id) => {
    this.setState(prevState => ({
      products: prevState.products.map((item) => {

        if (item.id === id) {
          if (name !== 'rate' && name !== 'count') {
            item[name] = product[name];
          }
          else {
            item.rating[name] = product.rating[name];
          }
        }
        return item;
      })
    }));
  }

  addProduct = (product) => {
    this.setState({
      products: [...this.state.products, product]
    })
  }

  deleteProduct = (requiredProduct) => {
    this.setState(prevState => ({
      products: prevState.products.filter(item => item !== requiredProduct)
    }));
  }

  fetchData(url) {
    axios(url)
      .then((response) => {
        this.setState({
          products: response.data,
          status: this.STATE_API.LOADED
        });
      })
      .catch((err) => {
        this.setState({
          status: this.STATE_API.ERROR
        });
      })
  }

  componentDidMount() {
    this.fetchData(this.URL)
  }

  render() {
    return (
      <div>
        {this.state.status === this.STATE_API.LOADING ? <PreLoader /> :
          // {}
          this.state.status === this.STATE_API.ERROR ? <Error />:
          <>
            <AddProduct addProduct={this.addProduct} />
            {this.state.products.length?<Products products={this.state.products} updateProduct={this.updateProduct} deleteProduct={this.deleteProduct} />:<>No Product Found</>}
            
          </>
        }
      </div>
    )
  }
}

